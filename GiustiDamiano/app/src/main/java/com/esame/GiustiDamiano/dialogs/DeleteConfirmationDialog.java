package com.esame.GiustiDamiano.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.esame.GiustiDamiano.R;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class DeleteConfirmationDialog extends DialogFragment {

    public interface IConfirmationDialogCallbacks {
        void onDeletionConfirmed(long id);
    }

    public static final String ID_KEY_FOR_BUNDLE = "idBundled";

    public static DeleteConfirmationDialog newInstance(long id) {
        DeleteConfirmationDialog deleteConfirmationDialog = new DeleteConfirmationDialog();
        Bundle bundle = new Bundle();
        bundle.putLong(ID_KEY_FOR_BUNDLE, id);
        deleteConfirmationDialog.setArguments(bundle);
        return deleteConfirmationDialog;
    }

    private IConfirmationDialogCallbacks listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IConfirmationDialogCallbacks)
            listener = (IConfirmationDialogCallbacks) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final long id = getArguments().getLong(ID_KEY_FOR_BUNDLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.deletion_confirmation_message))
                .setTitle(getString(R.string.deletion_confirmation_title))
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null)
                            listener.onDeletionConfirmed(id);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }
}
