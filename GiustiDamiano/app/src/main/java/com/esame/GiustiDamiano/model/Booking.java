package com.esame.GiustiDamiano.model;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class Booking {

    private long id;
    private String bookerName;
    private String phoneNumber;
    private int personsCount = 1; // default value as set in DB

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBookerName() {
        return bookerName;
    }

    public void setBookerName(String bookerName) {
        this.bookerName = bookerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPersonsCount() {
        return personsCount;
    }

    public void setPersonsCount(int personsCount) {
        this.personsCount = personsCount;
    }
}
