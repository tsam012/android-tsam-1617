package com.esame.GiustiDamiano;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.esame.GiustiDamiano.database.BookingsContentProvider;
import com.esame.GiustiDamiano.database.entities.EntitiesHelper;

public class InsertPersonsCountActivity extends Activity {

    private static final String PERSONS_COUNT_KEY_FOR_BUNDLE = "personsCountBundled";

    private EditText editTextPersonsCount;
    private Button btnCancel;
    private Button btnSave;
    private Button btnGoBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_persons_count);
        bindViews();

        if (savedInstanceState != null) {
            int personsCount = savedInstanceState.getInt(PERSONS_COUNT_KEY_FOR_BUNDLE, 1);
            editTextPersonsCount.setText("" + personsCount);
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHome();
            }
        });

        btnGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String count = editTextPersonsCount.getText().toString().trim();
                if (!TextUtils.isEmpty(count)) {
                    BookingData.getInstance().getBooking().setPersonsCount(Integer.valueOf(count));
                    saveBooking();
                } else {
                    Toast.makeText(InsertPersonsCountActivity.this, getString(R.string.missing_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String text = editTextPersonsCount.getText().toString().trim();
        if (!TextUtils.isEmpty(text))
            outState.putInt(PERSONS_COUNT_KEY_FOR_BUNDLE, Integer.valueOf(text));
    }

    private void bindViews() {
        editTextPersonsCount = (EditText) findViewById(R.id.editTextPersonsCount);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnGoBack = (Button) findViewById(R.id.btnGoBack);
    }

    private void saveBooking() {
        ContentValues contentValues = EntitiesHelper.contentValuesFromBooking(BookingData.getInstance().getBooking());
        getContentResolver().insert(BookingsContentProvider.BOOKINGS_URI, contentValues);
        launchHome();
    }

    private void launchHome() {
        Intent intent = new Intent(InsertPersonsCountActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        BookingData.getInstance().destroy();
    }
}
