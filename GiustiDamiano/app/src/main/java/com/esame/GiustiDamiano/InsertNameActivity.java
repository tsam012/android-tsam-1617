package com.esame.GiustiDamiano;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertNameActivity extends Activity {

    private static final String NEW_BOOKER_NAME_KEY_FOR_BUNDLE = "newBookerNameBundled";

    private EditText editTextNewBookerName;
    private Button btnCancel;
    private Button btnGoNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_name);
        bindViews();

        if (savedInstanceState == null) {
            BookingData.getInstance().init();
        } else {
            String bookerName = savedInstanceState.getString(NEW_BOOKER_NAME_KEY_FOR_BUNDLE, "");
            editTextNewBookerName.setText(bookerName);
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHome();
            }
        });

        btnGoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bookerName = editTextNewBookerName.getText().toString().trim();
                if (!TextUtils.isEmpty(bookerName)) {
                    BookingData.getInstance().getBooking().setBookerName(bookerName);

                    // start second activity
                    Intent intent = new Intent(InsertNameActivity.this, InsertPhoneNumberActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(InsertNameActivity.this, getString(R.string.missing_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(NEW_BOOKER_NAME_KEY_FOR_BUNDLE, editTextNewBookerName.getText().toString());
    }

    private void bindViews() {
        editTextNewBookerName = (EditText) findViewById(R.id.editTextNewBookerName);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnGoNext = (Button) findViewById(R.id.btnNext);
    }

    private void launchHome() {
        Intent intent = new Intent(InsertNameActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        BookingData.getInstance().destroy();
    }
}
