package com.esame.GiustiDamiano.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.esame.GiustiDamiano.R;
import com.esame.GiustiDamiano.database.entities.EntitiesHelper;
import com.esame.GiustiDamiano.model.Booking;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class BookingsCursorAdapter extends CursorAdapter {

    public BookingsCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.booking_list_item_layout, parent, false);

        BookingViewHolder viewHolder = new BookingViewHolder();
        viewHolder.txtBookerName = (TextView) view.findViewById(R.id.txtBookerName);
        viewHolder.txtPhoneNumber = (TextView) view.findViewById(R.id.txtPhoneNumber);
        viewHolder.txtPersonsCount = (TextView) view.findViewById(R.id.txtPersonsCount);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        BookingViewHolder viewHolder = (BookingViewHolder) view.getTag();

        Booking booking = EntitiesHelper.bookingFromCursor(cursor);

        viewHolder.txtBookerName.setText(booking.getBookerName());
        viewHolder.txtPhoneNumber.setText(booking.getPhoneNumber());
        viewHolder.txtPersonsCount.setText("" + booking.getPersonsCount());

    }

    private static class BookingViewHolder {
        private TextView txtBookerName;
        private TextView txtPhoneNumber;
        private TextView txtPersonsCount;
    }
}
