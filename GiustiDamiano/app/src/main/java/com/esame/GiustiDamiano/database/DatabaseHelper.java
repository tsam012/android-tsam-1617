package com.esame.GiustiDamiano.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.esame.GiustiDamiano.database.entities.BookingsTable;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "bookings.db";
    public static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BookingsTable.Statements.CREATE_TABLE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(BookingsTable.Statements.DROP_TABLE_STATEMENT);
        db.execSQL(BookingsTable.Statements.CREATE_TABLE_STATEMENT);
    }
}
