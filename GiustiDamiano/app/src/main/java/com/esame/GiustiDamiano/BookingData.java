package com.esame.GiustiDamiano;

import android.os.Bundle;
import android.text.TextUtils;

import com.esame.GiustiDamiano.model.Booking;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class BookingData {

    public static final String BOOKING_DATA_KEY_FOR_BUNDLE = "bookingDataBundled";

    private static final String BOOKING_ID_KEY_FOR_BUNDLE = "bookingIdBundled";
    private static final String BOOKER_NAME_KEY_FOR_BUNDLE = "bookerNameBundled";
    private static final String PHONE_NUMBER_KEY_FOR_BUNDLE = "phoneNumberBundled";
    private static final String PERSONS_COUNT_KEY_FOR_BUNDLE = "personsCountBundled";

    // --- singleton instance --- //
    private static BookingData instance = null;

    public static BookingData getInstance() {
        if (instance == null)
            instance = new BookingData();
        return instance;
    }
    // --- //

    private Booking booking;

    private BookingData() {

    }

    public void init() {
        booking = new Booking();
    }

    public void initFromBundle(Bundle bundle) {
        booking = new Booking();
        if (bundle.containsKey(BOOKING_ID_KEY_FOR_BUNDLE))
            booking.setId(bundle.getLong(BOOKING_ID_KEY_FOR_BUNDLE));
        if (bundle.containsKey(BOOKER_NAME_KEY_FOR_BUNDLE))
            booking.setBookerName(bundle.getString(BOOKER_NAME_KEY_FOR_BUNDLE));
        if (bundle.containsKey(PHONE_NUMBER_KEY_FOR_BUNDLE))
            booking.setPhoneNumber(PHONE_NUMBER_KEY_FOR_BUNDLE);
        if (bundle.containsKey(PERSONS_COUNT_KEY_FOR_BUNDLE))
            booking.setPersonsCount(bundle.getInt(PERSONS_COUNT_KEY_FOR_BUNDLE));

    }

    public Booking getBooking() {
        return booking;
    }

    public void destroy() {
        booking = null;
    }

    public Bundle toBundle() {
        if (booking == null)
            return null;

        Bundle bundle = new Bundle();
        if (booking.getId() > 0)
            bundle.putLong(BOOKING_ID_KEY_FOR_BUNDLE, booking.getId());
        if (!TextUtils.isEmpty(booking.getBookerName()))
            bundle.putString(BOOKER_NAME_KEY_FOR_BUNDLE, booking.getBookerName());
        if (!TextUtils.isEmpty(booking.getPhoneNumber()))
            bundle.putString(PHONE_NUMBER_KEY_FOR_BUNDLE, booking.getPhoneNumber());
        bundle.putInt(PERSONS_COUNT_KEY_FOR_BUNDLE, booking.getPersonsCount());
        return bundle;
    }
}
