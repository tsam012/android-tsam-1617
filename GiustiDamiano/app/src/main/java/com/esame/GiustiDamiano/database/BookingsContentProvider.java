package com.esame.GiustiDamiano.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.esame.GiustiDamiano.database.entities.BookingsTable;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class BookingsContentProvider extends ContentProvider {

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/bookings";
    public static final String ITEM_CONTENT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/booking";

    private static final String AUTHORITY = "com.esame.GiustiDamiano.database.BookingsContentProvider";
    private static final String BASE_PATH_BOOKINGS = "bookings";

    public static final Uri BOOKINGS_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_BOOKINGS);

    // uri matcher configuration
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int BOOKINGS = 1000;
    private static final int SINGLE_BOOKING = 2000;

    static {
        URI_MATCHER.addURI(AUTHORITY, BASE_PATH_BOOKINGS, BOOKINGS);
        URI_MATCHER.addURI(AUTHORITY, BASE_PATH_BOOKINGS + "/#", SINGLE_BOOKING);
    }

    private DatabaseHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(BookingsTable.TABLE_NAME);

        int uriType = URI_MATCHER.match(uri);
        switch (uriType) {
            case BOOKINGS: {
                //
            }
            break;
            case SINGLE_BOOKING: {
                String bookingId = uri.getLastPathSegment();
//                if (!TextUtils.isEmpty(selection)) {
//                    builder.appendWhere(BookingsTable._ID + " = " + bookingId + " AND " + selection);
//                } else {
                    builder.appendWhere(BookingsTable._ID + " = " + bookingId);
//                }
            }
            break;
            default:
                throw new IllegalArgumentException("Uri not implemented by provider: " + uri);
        }

        Cursor cursor = builder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int uriType = URI_MATCHER.match(uri);
        long insertedRowId = 0;
        switch (uriType) {
            case BOOKINGS: {
                insertedRowId = database.insert(BookingsTable.TABLE_NAME, null, values);
            }
            break;
            case SINGLE_BOOKING :
                throw new IllegalArgumentException("Uri does not support inserting: " + uri);
            default:
                throw new IllegalArgumentException("Uri for insertion not implemented by provider: " + uri);
        }

        Uri resultUri = null;
        if (insertedRowId != -1) {
            getContext().getContentResolver().notifyChange(uri, null);
            resultUri = Uri.parse(uri + "/" + insertedRowId);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return resultUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int uriType = URI_MATCHER.match(uri);
        int deletedRowsCount = 0;
        switch (uriType) {
            case BOOKINGS: {
                deletedRowsCount = database.delete(BookingsTable.TABLE_NAME, selection, selectionArgs);
            }
            break;
            case SINGLE_BOOKING: {
                String bookingId = uri.getLastPathSegment();
                if (!TextUtils.isEmpty(selection)) {
                    deletedRowsCount = database.delete(BookingsTable.TABLE_NAME,
                            String.format("%s = %s AND %s", BookingsTable._ID, bookingId),
                            selectionArgs);
                } else {
                    deletedRowsCount = database.delete(BookingsTable.TABLE_NAME,
                            String.format("%s = %s", BookingsTable._ID, bookingId),
                            null);
                }
            }
            break;
            default:
                throw new IllegalArgumentException("Uri for deletion not implemented by provider: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return deletedRowsCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int uriType = URI_MATCHER.match(uri);
        int updatedRowsCount = 0;
        switch (uriType) {
            case BOOKINGS:
                throw new IllegalArgumentException("Uri does not support update: " + uri);
            case SINGLE_BOOKING: {
                String bookingId = uri.getLastPathSegment();
                if (!TextUtils.isEmpty(selection)) {
                    updatedRowsCount = database.update(BookingsTable.TABLE_NAME, values,
                            String.format("%s = %s AND %s", BookingsTable._ID, bookingId, selection),
                            selectionArgs);
                } else {
                    updatedRowsCount = database.update(BookingsTable.TABLE_NAME, values,
                            String.format("%s = %s", BookingsTable._ID, bookingId),
                            null);
                }
            }
            break;
            default:
                throw new IllegalArgumentException("Uri for update not implemented by provider: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return updatedRowsCount;
    }
}
