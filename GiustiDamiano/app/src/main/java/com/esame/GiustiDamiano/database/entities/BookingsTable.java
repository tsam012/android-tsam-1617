package com.esame.GiustiDamiano.database.entities;

import android.provider.BaseColumns;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class BookingsTable implements BaseColumns {

    public static final String TABLE_NAME = "bookings";

    public static final String BOOKER_NAME_COLUMN_NAME = "bookerName";
    public static final String PHONE_NUMBER_COLUMN_NAME = "phoneNumber";
    public static final String PERSONS_COUNT_COLUMN_NAME = "personsCount";

    public static class Statements {
        public static final String CREATE_TABLE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                BOOKER_NAME_COLUMN_NAME + " TEXT NOT NULL, " +
                PHONE_NUMBER_COLUMN_NAME + " TEXT NOT NULL, " +
                PERSONS_COUNT_COLUMN_NAME + " INTEGER DEFAULT 1" +
                ")";

        public static final String DROP_TABLE_STATEMENT = "DROP TABLE " + TABLE_NAME;
    }
}
