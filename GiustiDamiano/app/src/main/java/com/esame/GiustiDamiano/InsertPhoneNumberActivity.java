package com.esame.GiustiDamiano;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertPhoneNumberActivity extends Activity {

    private static final String PHONE_NUMBER_KEY_FOR_BUNDLE = "phoneNumberBundled";

    private EditText editTextNewPhoneNumber;
    private Button btnCancel;
    private Button btnGoNext;
    private Button btnGoBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_phone_number);
        bindViews();

        if (savedInstanceState != null) {
            String phoneNumber = savedInstanceState.getString(PHONE_NUMBER_KEY_FOR_BUNDLE, "");
            editTextNewPhoneNumber.setText(phoneNumber);
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHome();
            }
        });

        btnGoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editTextNewPhoneNumber.getText().toString().trim();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    BookingData.getInstance().getBooking().setPhoneNumber(phoneNumber);

                    // start second activity
                    Intent intent = new Intent(InsertPhoneNumberActivity.this, InsertPersonsCountActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(InsertPhoneNumberActivity.this, getString(R.string.missing_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PHONE_NUMBER_KEY_FOR_BUNDLE, editTextNewPhoneNumber.getText().toString());
    }

    private void bindViews() {
        editTextNewPhoneNumber = (EditText) findViewById(R.id.editTextNewPhoneNumber);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnGoNext = (Button) findViewById(R.id.btnNext);
        btnGoBack = (Button) findViewById(R.id.btnGoBack);
    }

    private void launchHome() {
        Intent intent = new Intent(InsertPhoneNumberActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        BookingData.getInstance().destroy();
    }
}
