package com.esame.GiustiDamiano;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.esame.GiustiDamiano.adapters.BookingsCursorAdapter;
import com.esame.GiustiDamiano.database.BookingsContentProvider;
import com.esame.GiustiDamiano.dialogs.DeleteConfirmationDialog;

public class MainActivity extends Activity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        DeleteConfirmationDialog.IConfirmationDialogCallbacks {

    private static final int ADAPTER_LOADER_ID = 1;
    private static final String DELETE_CONFIRMATION_DIALOG_TAG = "deleteConfirmationDialog";

    private ListView bookingsListView;
    private Button btnAddBooking;

    private BookingsCursorAdapter bookingsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();

        bookingsAdapter = new BookingsCursorAdapter(getApplicationContext(), null);
        bookingsListView.setAdapter(bookingsAdapter);

        getLoaderManager().initLoader(ADAPTER_LOADER_ID, null, this);


        btnAddBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, InsertNameActivity.class);
                startActivity(intent);
            }
        });

        bookingsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showDeletionConfirmationDialog(id);
                return true;
            }
        });
    }

    private void bindViews() {
        bookingsListView = (ListView) findViewById(R.id.bookingsListView);
        btnAddBooking = (Button) findViewById(R.id.btnAddBooking);
    }

    private void showDeletionConfirmationDialog(long id) {
        DeleteConfirmationDialog.newInstance(id)
                .show(getFragmentManager(), DELETE_CONFIRMATION_DIALOG_TAG);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getApplicationContext(), BookingsContentProvider.BOOKINGS_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        bookingsAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        bookingsAdapter.swapCursor(null);
    }

    @Override
    public void onDeletionConfirmed(long id) {
        Uri uri = Uri.parse(BookingsContentProvider.BOOKINGS_URI + "/" + id);
        getContentResolver().delete(uri, null, null);
    }
}
