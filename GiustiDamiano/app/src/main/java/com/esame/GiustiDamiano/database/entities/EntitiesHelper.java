package com.esame.GiustiDamiano.database.entities;

import android.content.ContentValues;
import android.database.Cursor;

import com.esame.GiustiDamiano.model.Booking;

/**
 * Created by Damiano Giusti on 24/02/17.
 */
public class EntitiesHelper {

    public static Booking bookingFromCursor(Cursor cursor) {
        Booking booking = new Booking();
        booking.setId(cursor.getLong(cursor.getColumnIndex(BookingsTable._ID)));
        booking.setBookerName(cursor.getString(cursor.getColumnIndex(BookingsTable.BOOKER_NAME_COLUMN_NAME)));
        booking.setPhoneNumber(cursor.getString(cursor.getColumnIndex(BookingsTable.PHONE_NUMBER_COLUMN_NAME)));
        booking.setPersonsCount(cursor.getInt(cursor.getColumnIndex(BookingsTable.PERSONS_COUNT_COLUMN_NAME)));

        return booking;
    }

    public static ContentValues contentValuesFromBooking(Booking booking) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BookingsTable.BOOKER_NAME_COLUMN_NAME, booking.getBookerName());
        contentValues.put(BookingsTable.PHONE_NUMBER_COLUMN_NAME, booking.getPhoneNumber());
        contentValues.put(BookingsTable.PERSONS_COUNT_COLUMN_NAME, booking.getPersonsCount());
        return contentValues;
    }
}
