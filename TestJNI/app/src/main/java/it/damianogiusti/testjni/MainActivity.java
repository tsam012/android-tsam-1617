package it.damianogiusti.testjni;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        long time = System.nanoTime();
        sum(randomNumber(), randomNumber());
        Log.d(TAG, "native: " + (System.nanoTime() - time));

        time = System.nanoTime();
        jsum(jrandomNumber(), jrandomNumber());
        Log.d(TAG, "java: " + (System.nanoTime() - time));
//        tv.setText(String.format("%s, we calculated this: %d", stringFromJNI(), ));

    }

    private int jsum(int a, int b) {
        int sum = 0;
        int i = 0;
        for (i=0; i<10000; ++i)
            sum += i;
        return sum;
    }

    private int jrandomNumber() {
        return (int) Math.round(Math.random() * 10);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native int sum(int a, int b);

    public native int randomNumber();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
}
