#include <jni.h>
#include <string>
#include "stdlib.h"


extern "C"
jstring
Java_it_damianogiusti_testjni_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
jint
Java_it_damianogiusti_testjni_MainActivity_sum(JNIEnv, jobject,
                                               jint a, jint b) {
    jint sum = 0;
    int i = 0;
    for (i=0; i<10000; ++i)
        sum += i;
    return sum;
}

extern "C"
jint
Java_it_damianogiusti_testjni_MainActivity_randomNumber(JNIEnv, jobject) {
    srand(time(NULL));
    return rand() % 100;
}
