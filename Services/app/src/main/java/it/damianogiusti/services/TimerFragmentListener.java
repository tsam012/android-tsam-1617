package it.damianogiusti.services;

/**
 * Created by Damiano Giusti on 16/06/17.
 */
public interface TimerFragmentListener {

    void onProgress(int progress);
}
