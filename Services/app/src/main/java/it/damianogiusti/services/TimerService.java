package it.damianogiusti.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import java.lang.ref.WeakReference;
import java.util.UUID;

/**
 * Created by Damiano Giusti on 16/06/17.
 */
public class TimerService extends Service {

    /*

    TAEG
    Tassuo annuo effettivo globale

     */

    private TimerService randomServiceBinder = new TimerServiceBinder();


    public static final String TIMER_PROGRESS = UUID.randomUUID().toString();
    public static final String STOP_SIGNAL = UUID.randomUUID().toString();

    private TimerAsyncTask timerAsyncTask;
    private LocalBroadcastManager broadcastManager;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(STOP_SIGNAL)) {
                stopSelf();
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastManager.registerReceiver(broadcastReceiver, new IntentFilter(STOP_SIGNAL));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (timerAsyncTask == null) {
            timerAsyncTask = new TimerAsyncTask(this);
            timerAsyncTask.execute();
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }

    private void onProgress(int progress) {
        Intent intent = new Intent(TIMER_PROGRESS);
        intent.putExtra(TIMER_PROGRESS, progress);
        broadcastManager.sendBroadcast(intent);
    }

    public class TimerServiceBinder extends Binder {
        public TimerService getService() {
            return TimerService.this;
        }
    }

    private static class TimerAsyncTask extends AsyncTask<Void, Integer, Void> {

        private WeakReference<TimerService> service;
        private int counter = 0;

        public TimerAsyncTask(TimerService service) {
            this.service = new WeakReference<>(service);
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (!isCancelled() && service.get() != null) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //
                }
                publishProgress(++counter);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            TimerService service = this.service.get();
            if (service != null) {
                service.onProgress(values[0]);
            }
        }
    }
}
