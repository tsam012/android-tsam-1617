package it.damianogiusti.services;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ServiceConnection serviceConnection;

    private RandomService randomService;
    private boolean isBindToService = false;

    private Button btnNext;
    private TextView txtNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serviceConnection = createServiceConnection();
        bindViews();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (randomService != null)
                    txtNext.setText(String.valueOf(randomService.getRandomNumber()));
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        TimerFragment fragment = (TimerFragment) fragmentManager.findFragmentByTag(TimerFragment.class.getSimpleName());
        if (fragment == null) {
            fragment = new TimerFragment();
            fragmentManager.beginTransaction()
                    .add(fragment, TimerFragment.class.getSimpleName())
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.bindService(new Intent(MainActivity.this, RandomService.class), serviceConnection, BIND_AUTO_CREATE);
        this.isBindToService = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.unbindService(serviceConnection);
        this.isBindToService = false;
    }

    private void bindViews() {
        btnNext = (Button) findViewById(R.id.btnNextInt);
        txtNext = (TextView) findViewById(R.id.txtNumber);
    }

    private void onProgress(int progress) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private ServiceConnection createServiceConnection() {
        return new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                RandomService.RandomServiceBinder binder = (RandomService.RandomServiceBinder) service;
                randomService = binder.getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                randomService = null;
            }
        };
    }

}
