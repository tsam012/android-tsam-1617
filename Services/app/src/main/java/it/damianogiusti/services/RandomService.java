package it.damianogiusti.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Random;

/**
 * Created by Damiano Giusti on 13/01/17.
 */
public class RandomService extends Service {

    private RandomServiceBinder randomServiceBinder = new RandomServiceBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    public int getRandomNumber() {
        Random random = new Random();
        return random.nextInt();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return randomServiceBinder;
    }

    public class RandomServiceBinder extends Binder {
        public RandomService getService() {
            return RandomService.this;
        }
    }

    private void onProgress(int counter) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(this.getClass().getSimpleName(), "Servizio distrutto");
    }


}
