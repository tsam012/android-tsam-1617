package it.damianogiusti.services;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by Damiano Giusti on 16/06/17.
 */
public class TimerFragment extends Fragment {

    private ServiceConnection serviceConnection;
    private RandomService randomService;

    private TimerAsyncTask timerAsyncTask;
    private TimerFragmentListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof TimerFragmentListener) {
            listener = (TimerFragmentListener) activity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        return null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        serviceConnection = createServiceConnection();
        getActivity().bindService(new Intent(getActivity(), RandomService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unbindService(serviceConnection);
    }

    private void onProgress(int counter) {
        if (listener != null) {
            listener.onProgress(counter);
        }
    }

    private ServiceConnection createServiceConnection() {
        return new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                RandomService.RandomServiceBinder binder = (RandomService.RandomServiceBinder) service;
                randomService = binder.getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                randomService = null;
            }
        };
    }

    private static class TimerAsyncTask extends AsyncTask<Void, Integer, Void> {

        private WeakReference<TimerFragment> service;
        private int counter = 0;

        public TimerAsyncTask(TimerFragment service) {
            this.service = new WeakReference<>(service);
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (!isCancelled() && service.get() != null) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //
                }
                publishProgress(++counter);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            TimerFragment service = this.service.get();
            if (service != null) {
                service.onProgress(values[0]);
            }
        }
    }
}
