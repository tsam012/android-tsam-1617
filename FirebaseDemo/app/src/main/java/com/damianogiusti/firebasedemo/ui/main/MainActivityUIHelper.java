package com.damianogiusti.firebasedemo.ui.main;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.damianogiusti.firebasedemo.BaseActivity;
import com.damianogiusti.firebasedemo.R;
import com.damianogiusti.firebasedemo.model.User;
import com.damianogiusti.firebasedemo.utils.Constants;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

abstract class MainActivityUIHelper extends BaseActivity {

    protected Toolbar toolbar;
    private AccountHeader accountHeader;
    private Drawer drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFabClicked();
            }
        });
    }

    protected final void initNavigationDrawer(User user, Bundle savedInstanceState) {
        IProfile profile = new ProfileDrawerItem()
                .withName(user.getUserName())
                .withIcon(user.getPhotoUrl())
                .withEmail(user.getEmailAddress())
                .withIdentifier(Constants.PROFILE);

        accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .addProfiles(profile)
                .withHeaderBackground(R.drawable.drawer_background)
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .withActionBarDrawerToggle(true)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withName(R.string.notes)
                                .withIcon(GoogleMaterial.Icon.gmd_view_list)
                                .withIdentifier(Constants.NOTES),
                        new PrimaryDrawerItem()
                                .withName(R.string.categories)
                                .withIcon(GoogleMaterial.Icon.gmd_folder)
                                .withIdentifier(Constants.CATEGORIES),
                        new PrimaryDrawerItem()
                                .withName(R.string.settings)
                                .withIcon(GoogleMaterial.Icon.gmd_settings)
                                .withIdentifier(Constants.SETTINGS),
                        new PrimaryDrawerItem()
                                .withName(R.string.logout)
                                .withIcon(GoogleMaterial.Icon.gmd_exit_to_app)
                                .withIdentifier(Constants.LOGOUT)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem != null && drawerItem instanceof Nameable) {
                            Nameable item = (Nameable) drawerItem;
                            toolbar.setTitle(item.getName().getText(MainActivityUIHelper.this));
                        }
                        if (drawerItem != null) {
                            onTouchItem((int) drawerItem.getIdentifier());
                        }
                        return false;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {

                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withFireOnInitialOnClick(true)
                .withSavedInstance(savedInstanceState)
                .build();

        drawer.addStickyFooterItem(new PrimaryDrawerItem()
                .withName(R.string.delete_account)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        onDeleteItemPressed();
                        return false;
                    }
                })
                .withIcon(GoogleMaterial.Icon.gmd_delete));
    }

    private void onTouchItem(int identifier) {
        switch (identifier) {
            case Constants.NOTES: {
                onNotesItemClicked();
            }
            break;
            case Constants.CATEGORIES: {
                onCategoriesItemClicked();
            }
            break;
            case Constants.SETTINGS: {
                onSettingsItemClicked();
            }
            break;
            case Constants.LOGOUT: {
                onLogoutItemClicked();
            }
            default: {
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected abstract void onFabClicked();

    protected abstract void onNotesItemClicked();

    protected abstract void onCategoriesItemClicked();

    protected abstract void onSettingsItemClicked();

    protected abstract void onLogoutItemClicked();

    protected abstract void onDeleteItemPressed();
}
