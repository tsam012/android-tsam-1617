package com.damianogiusti.firebasedemo;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by Damiano Giusti on 15/06/17.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new FirebaseLog(), new Timber.DebugTree());
        } else {
            Timber.plant(new FirebaseLog());
        }
    }
}
