package com.damianogiusti.firebasedemo.model;

/**
 * Created by Damiano Giusti on 20/06/17.
 */
public class User {
    private String userName;
    private String photoUrl;
    private String emailAddress;

    public static User anonymous() {
        User user = new User();
        user.userName = "Anonymous";
        user.photoUrl = "";
        user.emailAddress = "";
        return user;
    }

    public User() { }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
