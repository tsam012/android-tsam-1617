package com.damianogiusti.firebasedemo;

import android.support.annotation.StringRes;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by Damiano Giusti on 20/06/17.
 */
public class BaseActivity extends AppCompatActivity {

    protected void toast(@StringRes int stringResId) {
        toast(str(stringResId));
    }

    protected void toast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    protected void snackbar(@StringRes int stringResId) {
        snackbar(str(stringResId));
    }

    protected void snackbar(String text) {
        Snackbar.make(getWindow().getDecorView(), text, BaseTransientBottomBar.LENGTH_SHORT).show();
    }

    protected String str(@StringRes int stringResId, Object... args) {
        return getString(stringResId, args);
    }
}
