package com.damianogiusti.firebasedemo.utils;

/**
 * Created by Damiano Giusti on 20/06/17.
 */
public class Constants {

    public static final int PROFILE = 1000;
    public static final int NOTES = 1010;
    public static final int CATEGORIES = 1020;
    public static final int SETTINGS = 1030;
    public static final int LOGOUT = 1040;
}
