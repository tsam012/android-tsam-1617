package com.damianogiusti.firebasedemo.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.damianogiusti.firebasedemo.R;
import com.damianogiusti.firebasedemo.model.User;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class MainActivity extends MainActivityUIHelper implements FirebaseAuth.AuthStateListener {

    private static final int SIGN_IN_REQUEST_CODE = 733;

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser firebaseUser;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser == null) {
            login();
        } else {
            user = createUser(firebaseUser);
        }

        if (user == null) {
            user = User.anonymous();
        }
        initNavigationDrawer(user, savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(this);
    }

    @Override
    protected void onFabClicked() {

    }

    @Override
    protected void onNotesItemClicked() {

    }

    @Override
    protected void onCategoriesItemClicked() {

    }

    @Override
    protected void onSettingsItemClicked() {

    }

    @Override
    protected void onLogoutItemClicked() {
        logout();
    }

    @Override
    protected void onDeleteItemPressed() {
        deleteUser();
    }

    private User createUser(FirebaseUser firebaseUser) {
        User user = new User();
        user.setUserName(firebaseUser.getDisplayName());
        user.setEmailAddress(firebaseUser.getEmail());
        if (firebaseUser.getPhotoUrl() != null) {
            user.setPhotoUrl(firebaseUser.getPhotoUrl().toString());
        } else {
            user.setPhotoUrl("");
        }
        return user;
    }

    private void login() {
        startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build()))
                        .build(),
                SIGN_IN_REQUEST_CODE);
    }

    private void logout() {
        AuthUI.getInstance().signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            login();
                        } else {
                            toast(R.string.logout_failure);
                        }
                    }
                });
    }

    private void deleteUser() {
        AuthUI.getInstance().delete(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            login();
                        } else {
                            toast(R.string.account_deletion_failure);
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN_REQUEST_CODE) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            switch (resultCode) {
                case ResultCodes.OK: {

                }
                break;
                case ErrorCodes.NO_NETWORK: {
                    snackbar(R.string.error_no_network);
                }
                break;
                case ErrorCodes.UNKNOWN_ERROR: {
                    snackbar(R.string.generic_error);
                }
                break;
            }
        }
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            initNavigationDrawer(createUser(currentUser), null);
        }
    }
}
