package com.damianogiusti.firebasedemo;

import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import timber.log.Timber;

/**
 * Created by Damiano Giusti on 15/06/17.
 */
public class FirebaseLog extends Timber.Tree {

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return;
        }
        String type = "";
        switch (priority) {
            case Log.INFO:
                type = "INFO";
                break;
            case Log.WARN:
                type = "WARN";
                break;
            case Log.ERROR:
                type = "ERROR";
                break;
            case Log.ASSERT:
                type = "ASSERT";
        }

        FirebaseCrash.log(String.format("[%s] %s", type, message));

        if (t != null) {
            FirebaseCrash.report(t);
        }
    }
}
