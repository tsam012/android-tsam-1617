package com.damianogiusti.firebasedemo.model;

/**
 * Created by Damiano Giusti on 20/06/17.
 */
public class Category {

    private String categoryId;
    private String categoryName;
    private int count;

    public Category() { }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
