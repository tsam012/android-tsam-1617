package com.damianogiusti.cronometro.ui.chronometer;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class TimerFragment extends Fragment {

    public static final String TAG = "TimerFragment";

    private TimerTask timerTask;
    private TimerListener listener;

    public static TimerFragment newInstance() {
        Bundle args = new Bundle();

        TimerFragment fragment = new TimerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        timerTask = new TimerTask(this);
        timerTask.execute();

        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof TimerListener) {
            listener = (TimerListener) activity;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void stop() {
        timerTask.stop();
    }

    public boolean isStopped() {
        return timerTask.isStopped;
    }

    private void onTimerTick(int progress) {
        if (listener != null) {
            listener.onTimerTick(progress);
        }
    }

    private static class TimerTask extends AsyncTask<Void, Integer, Void> {

        private WeakReference<TimerFragment> weakFragment;
        private boolean isStopped = false;
        private int counter = 0;

        public TimerTask(TimerFragment timerFragment) {
            this.weakFragment = new WeakReference<>(timerFragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (!isStopped && weakFragment.get() != null) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    isStopped = true;
                    break;
                }
                publishProgress(++counter);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            TimerFragment fragment = weakFragment.get();
            if (fragment != null) {
                fragment.onTimerTick(values[0]);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        void stop() {
            isStopped = true;
        }
    }
}
