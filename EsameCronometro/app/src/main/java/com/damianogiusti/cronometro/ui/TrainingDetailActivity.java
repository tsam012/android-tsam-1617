package com.damianogiusti.cronometro.ui;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.damianogiusti.cronometro.R;

public class TrainingDetailActivity extends Activity {

    private TextView locationTextView;
    private TextView totalTimeTextView;
    private TextView lapCountTextView;
    private ListView lapsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_detail);
        bindViews();
    }

    private void bindViews() {
        locationTextView = (TextView) findViewById(R.id.locationTextView);
        totalTimeTextView = (TextView) findViewById(R.id.totalTimeTextView);
        lapCountTextView = (TextView) findViewById(R.id.lapCountTextView);
        lapsListView = (ListView) findViewById(R.id.lapsListView);
    }
}
