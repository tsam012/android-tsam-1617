package com.damianogiusti.cronometro.ui.trainingslist;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.damianogiusti.cronometro.R;
import com.damianogiusti.cronometro.database.model.Training;
import com.damianogiusti.cronometro.database.model.TrainingHelper;
import com.damianogiusti.cronometro.ui.utils.Utils;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class TrainingsAdapter extends CursorAdapter {

    private LayoutInflater inflater;

    public TrainingsAdapter(Context context, Cursor cursor) {
        super(context, cursor, false);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = inflater.inflate(R.layout.training_cell_layout, parent, false);
        TrainingViewHolder viewHolder = new TrainingViewHolder();
        viewHolder.dateTextView = (TextView) view.findViewById(R.id.dateTextView);
        viewHolder.durationTextView = (TextView) view.findViewById(R.id.durationTextView);
        viewHolder.locationTextView = (TextView) view.findViewById(R.id.locationTextView);
        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TrainingViewHolder viewHolder = (TrainingViewHolder) view.getTag();
        Training training = TrainingHelper.fromCursor(cursor);
        viewHolder.locationTextView.setText(training.getLocation());
        viewHolder.durationTextView.setText(Utils.formatTime((int) training.getTotDurationMillis() / 1000));
        viewHolder.dateTextView.setText(Utils.formatDate(training.getDate()));
    }

    private static class TrainingViewHolder {
        TextView locationTextView;
        TextView dateTextView;
        TextView durationTextView;
    }
}
