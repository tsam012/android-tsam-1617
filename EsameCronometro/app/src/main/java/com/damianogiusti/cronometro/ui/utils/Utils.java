package com.damianogiusti.cronometro.ui.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class Utils {

    public static String formatTime(int time) {
        return formatTime(true, time);
    }

    public static String formatTime(boolean minutesLeadingZero, int time) {
        if (minutesLeadingZero) {
            return String.format("%02d:%02d", time / 60, Math.round(time % 60));
        } else {
            return String.format("%d:%02d", time / 60, Math.round(time % 60));
        }
    }

    public static String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }
}
