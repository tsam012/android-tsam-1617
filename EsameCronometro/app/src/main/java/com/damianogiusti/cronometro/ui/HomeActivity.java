package com.damianogiusti.cronometro.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.damianogiusti.cronometro.R;
import com.damianogiusti.cronometro.ui.trainingslist.TrainingsListActivity;

public class HomeActivity extends Activity {

    private TextView trainingCountTextView;
    private Button newTrainingButton;
    private Button trainingListButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindViews();

        newTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateNewTrainingPressed();
            }
        });

        trainingListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowTrainingListPressed();
            }
        });
    }

    private void onCreateNewTrainingPressed() {
        Intent intent = new Intent(this, InsertLocationActivity.class);

        startActivity(intent);
    }

    private void onShowTrainingListPressed() {
        Intent intent = new Intent(this, TrainingsListActivity.class);

        startActivity(intent);
    }

    private void bindViews() {
        trainingCountTextView = (TextView) findViewById(R.id.trainingCountTextView);
        newTrainingButton = (Button) findViewById(R.id.newTrainingButton);
        trainingListButton = (Button) findViewById(R.id.trainingListButton);
    }
}
