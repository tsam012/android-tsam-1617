package com.damianogiusti.cronometro.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.damianogiusti.cronometro.database.helper.DatabaseHelper;
import com.damianogiusti.cronometro.database.model.LapHelper;
import com.damianogiusti.cronometro.database.model.TrainingHelper;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class ChronometerContentProvider extends ContentProvider {

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/trainings";
    public static final String ITEM_CONTENT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/training";

    private static final String AUTHORITY = "com.damianogiusti.cronometro.database";
    private static final String BASE_PATH_TRAININGS = "trainings";
    private static final String BASE_PATH_LAPS = "laps";
    private static final String BASE_PATH_LAPS_FOR_TRAINING = "laps/trainingId";

    public static final Uri TRAININGS_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_TRAININGS);
    public static final Uri LAPS_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_LAPS);
    public static final Uri LAPS_FOR_TRAINING_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_LAPS_FOR_TRAINING);

    // uri matcher configuration
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int TRAININGS = 1000;
    private static final int SINGLE_TRAINING = 2000;
    private static final int LAPS = 3000;
    private static final int SINGLE_LAP = 4000;
    private static final int LAPS_FOR_TRAINING = 4100;

    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH_TRAININGS, TRAININGS);
        uriMatcher.addURI(AUTHORITY, BASE_PATH_TRAININGS + "/#", SINGLE_TRAINING);
        uriMatcher.addURI(AUTHORITY, BASE_PATH_LAPS, LAPS);
        uriMatcher.addURI(AUTHORITY, BASE_PATH_LAPS + "/#", SINGLE_LAP);
        uriMatcher.addURI(AUTHORITY, BASE_PATH_LAPS_FOR_TRAINING + "/#", LAPS_FOR_TRAINING);
    }

    private DatabaseHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)) {
            case TRAININGS: {
                queryBuilder.setTables(TrainingHelper.TABLE_NAME);
            }
            break;
            case SINGLE_TRAINING: {
                queryBuilder.setTables(TrainingHelper.TABLE_NAME);
                queryBuilder.appendWhere(String.format("%s = %s", TrainingHelper._ID, uri.getLastPathSegment()));
            }
            break;

            case LAPS: {
                queryBuilder.setTables(LapHelper.INNER_JOIN_WITH_TRAININGS);
            }
            break;
            case SINGLE_LAP: {
                queryBuilder.setTables(LapHelper.INNER_JOIN_WITH_TRAININGS);
                queryBuilder.appendWhere(String.format("%s = %s", LapHelper._ID, uri.getLastPathSegment()));
            }
            break;
            case LAPS_FOR_TRAINING: {
                queryBuilder.setTables(LapHelper.INNER_JOIN_WITH_TRAININGS);
                queryBuilder.appendWhere(String.format("%s = %s", LapHelper.TRAINING_ID, uri.getLastPathSegment()));
            }
            break;
            default: {
                throwNotImplemented(uri);
            }
        }

        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        long insertedId = 0;
        String basePath = "";

        switch (uriMatcher.match(uri)) {
            case TRAININGS: {
                insertedId = database.insert(TrainingHelper.TABLE_NAME, null, values);
                basePath = BASE_PATH_TRAININGS;
            }
            break;
            case LAPS: {
                insertedId = database.insert(LapHelper.TABLE_NAME, null, values);
                basePath = BASE_PATH_LAPS;
            }
            break;
            default: {
                throwNotImplemented(uri);
            }
        }
        if (insertedId >= 0) {
            notifyChanged(uri);
            return Uri.parse("content://" + AUTHORITY + "/" + basePath + "/" + insertedId);
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        int deletionCount = 0;

        switch (uriMatcher.match(uri)) {
            case TRAININGS: {
                deletionCount = database.delete(TrainingHelper.TABLE_NAME, selection, selectionArgs);
            }
            break;
            case SINGLE_TRAINING: {
                String whereClause = String.format("%s = %s", TrainingHelper._ID, uri.getLastPathSegment());
                if (TextUtils.isEmpty(selection)) {
                    deletionCount = database.delete(TrainingHelper.TABLE_NAME, whereClause, null);
                } else {
                    deletionCount = database.delete(TrainingHelper.TABLE_NAME,
                            String.format("%s AND %s", whereClause, selection), selectionArgs);
                }
            }
            break;

            case LAPS: {
                deletionCount = database.delete(LapHelper.TABLE_NAME, selection, selectionArgs);
            }
            break;
            case SINGLE_LAP: {
                String whereClause = String.format("%s = %s", LapHelper._ID, uri.getLastPathSegment());
                if (TextUtils.isEmpty(selection)) {
                    deletionCount = database.delete(LapHelper.TABLE_NAME, whereClause, null);
                } else {
                    deletionCount = database.delete(LapHelper.TABLE_NAME,
                            String.format("%s AND %s", whereClause, selection), selectionArgs);
                }
            }
            break;
            case LAPS_FOR_TRAINING: {
                String whereClause = String.format("%s = %s", LapHelper.TRAINING_ID, uri.getLastPathSegment());
                if (TextUtils.isEmpty(selection)) {
                    deletionCount = database.delete(LapHelper.TABLE_NAME, whereClause, null);
                } else {
                    deletionCount = database.delete(LapHelper.TABLE_NAME,
                            String.format("%s AND %s", whereClause, selection), selectionArgs);
                }
            }
            break;
            default: {
                throwNotImplemented(uri);
            }
        }

        if (deletionCount > 0) {
            notifyChanged(uri);
        }
        return deletionCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        int updatedCount = 0;

        switch (uriMatcher.match(uri)) {
            case TRAININGS: {
                updatedCount = database.update(TrainingHelper.TABLE_NAME, values, selection, selectionArgs);
            }
            break;
            case SINGLE_TRAINING: {
                String whereClause = String.format("%s = %s", TrainingHelper._ID, uri.getLastPathSegment());
                if (TextUtils.isEmpty(selection)) {
                    updatedCount = database.delete(TrainingHelper.TABLE_NAME, whereClause, null);
                } else {
                    updatedCount = database.delete(TrainingHelper.TABLE_NAME,
                            String.format("%s AND %s", whereClause, selection), selectionArgs);
                }
            }
            break;

            case LAPS: {
                updatedCount = database.update(LapHelper.TABLE_NAME, values, selection, selectionArgs);
            }
            break;
            case SINGLE_LAP: {
                String whereClause = String.format("%s = %s", LapHelper._ID, uri.getLastPathSegment());
                if (TextUtils.isEmpty(selection)) {
                    updatedCount = database.update(LapHelper.TABLE_NAME, values, whereClause, null);
                } else {
                    updatedCount = database.update(LapHelper.TABLE_NAME, values,
                            String.format("%s AND %s", whereClause, selection), selectionArgs);
                }
            }
            break;
            default: {
                throwNotImplemented(uri);
            }
        }

        if (updatedCount > 0) {
            notifyChanged(uri);
        }
        return updatedCount;
    }

    // Utils

    private void throwNotImplemented(Uri uri) {
        throw new IllegalStateException(String.format("Uri %s not implemented.", uri));
    }

    private void notifyChanged(Uri uri) {
        getContext().getContentResolver().notifyChange(uri, null);
    }
}
