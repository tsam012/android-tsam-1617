package com.damianogiusti.cronometro.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.damianogiusti.cronometro.R;
import com.damianogiusti.cronometro.ui.chronometer.ChronometerActivity;

public class InsertLocationActivity extends Activity {

    private static final String LOCATION_NAME_KEY_FOR_BUNDLE = "locationNameBundled";

    private Button startButton;
    private EditText locationNameEditText;

    private String locationName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_location);
        bindViews();

        if (savedInstanceState != null) {
            locationName = savedInstanceState.getString(LOCATION_NAME_KEY_FOR_BUNDLE, "");
        }

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartButtonPressed();
            }
        });
    }

    private void bindViews() {
        startButton = (Button) findViewById(R.id.startButton);
        locationNameEditText = (EditText) findViewById(R.id.locationNameEditText);
    }

    private void onStartButtonPressed() {
        locationName = locationNameEditText.getText().toString();
        String name = locationName.trim();
        if (!TextUtils.isEmpty(name)) {
            Intent intent = new Intent(this, ChronometerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(ChronometerActivity.LOCATION_NAME_KEY_FOR_BUNDLE, name);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}
