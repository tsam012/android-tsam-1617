package com.damianogiusti.cronometro.ui.chronometer;

import android.content.Context;
import android.net.Uri;

import com.damianogiusti.cronometro.database.ChronometerContentProvider;
import com.damianogiusti.cronometro.database.model.Lap;
import com.damianogiusti.cronometro.database.model.LapHelper;
import com.damianogiusti.cronometro.database.model.Training;
import com.damianogiusti.cronometro.database.model.TrainingHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class ChronometerController {

    private Context context;

    public void create(Context context) {
        this.context = context;
    }

    public void chronometerStopped(int time, ArrayList<Integer> laps, String locationName) {
        Training training = new Training();
        training.setTotDurationMillis(time * 1000);
        training.setLocation(locationName);
        training.setDate(new Date());

        Uri trainingUri = context.getContentResolver().insert(ChronometerContentProvider.TRAININGS_URI,
                TrainingHelper.toContentValues(training));
        if (trainingUri != null) {
            training.setId(Long.parseLong(trainingUri.getLastPathSegment()));

            for (int lapTime : laps) {
                Lap lap = new Lap();
                lap.setDate(new Date());
                lap.setTraining(training);
                lap.setDurationMillis(lapTime);
                context.getContentResolver().insert(ChronometerContentProvider.LAPS_URI,
                        LapHelper.toContentValues(lap));
            }
        }
    }
}
