package com.damianogiusti.cronometro.database.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.damianogiusti.cronometro.database.model.LapHelper;
import com.damianogiusti.cronometro.database.model.TrainingHelper;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "chronometer.db";
    public static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createAll(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        deleteAll(db);
        createAll(db);
        Log.i(TAG, String.format("Upgrading DB from version %d to %d", oldVersion, newVersion));
    }

    // Utils

    private void createAll(SQLiteDatabase db) {
        db.execSQL(TrainingHelper.CREATE_TABLE_STATEMENT);
        db.execSQL(LapHelper.CREATE_TABLE_STATEMENT);
    }

    private void deleteAll(SQLiteDatabase db) {
        db.execSQL(LapHelper.DROP_TABLE_STATEMENT);
        db.execSQL(TrainingHelper.DROP_TABLE_STATEMENT);
    }
}
