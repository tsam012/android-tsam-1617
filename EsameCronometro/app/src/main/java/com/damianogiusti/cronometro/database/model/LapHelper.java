package com.damianogiusti.cronometro.database.model;

import android.content.ContentValues;
import android.provider.BaseColumns;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class LapHelper implements BaseColumns {

    public static final String TABLE_NAME = "laps";

    public static final String DATE_MILLIS = "timestamp";
    public static final String DURATION_MILLIS = "duration";
    public static final String TRAINING_ID = "trainingId";

    public static final String CREATE_TABLE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "(" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DATE_MILLIS + " INTEGER, " +
            DURATION_MILLIS + " INTEGER, " +
            TRAINING_ID + " INTEGER, " +
            "FOREIGN KEY (" + TRAINING_ID + ") REFERENCES " + TrainingHelper.TABLE_NAME + "(" + TrainingHelper._ID + ")" +
            ")";

    public static final String DROP_TABLE_STATEMENT = "DROP TABLE " + TABLE_NAME;

    public static final String INNER_JOIN_WITH_TRAININGS = TABLE_NAME + " INNER JOIN " + TrainingHelper.TABLE_NAME +
            " ON " + TrainingHelper.TABLE_NAME + "." + TrainingHelper._ID + " = " + TABLE_NAME + "." + _ID;

    public static ContentValues toContentValues(Lap lap) {
        ContentValues cv = new ContentValues();
        cv.put(DATE_MILLIS, lap.getDate().getTime());
        cv.put(DURATION_MILLIS, lap.getDurationMillis());
        cv.put(TRAINING_ID, lap.getTraining().getId());
        return cv;
    }
}
