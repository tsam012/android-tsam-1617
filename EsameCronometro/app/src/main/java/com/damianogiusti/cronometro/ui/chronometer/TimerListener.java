package com.damianogiusti.cronometro.ui.chronometer;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public interface TimerListener {

    void onTimerTick(int progress);
}
