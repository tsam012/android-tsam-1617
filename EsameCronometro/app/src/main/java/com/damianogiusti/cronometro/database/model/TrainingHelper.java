package com.damianogiusti.cronometro.database.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import java.util.Date;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class TrainingHelper implements BaseColumns {

    public static final String TABLE_NAME = "trainings";

    public static final String DATE_MILLIS = "timestamp";
    public static final String LOCATION = "location";
    public static final String TOTAL_DURATION_MILLIS = "duration";

    public static final String CREATE_TABLE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "(" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DATE_MILLIS + " INTEGER NOT NULL, " +
            LOCATION + " TEXT NOT NULL, " +
            TOTAL_DURATION_MILLIS + " INTEGER NOT NULL" +
            ")";

    public static final String DROP_TABLE_STATEMENT = "DROP TABLE " + TABLE_NAME;

    public static ContentValues toContentValues(Training training) {
        ContentValues cv = new ContentValues();
        cv.put(DATE_MILLIS, training.getDate().getTime());
        cv.put(LOCATION, training.getLocation());
        cv.put(TOTAL_DURATION_MILLIS, training.getTotDurationMillis());
        return cv;
    }

    public static Training fromCursor(Cursor cursor) {
        Training training = new Training();
        training.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
        training.setLocation(cursor.getString(cursor.getColumnIndex(LOCATION)));
        training.setDate(new Date(cursor.getLong(cursor.getColumnIndex(DATE_MILLIS))));
        training.setTotDurationMillis(cursor.getLong(cursor.getColumnIndex(TOTAL_DURATION_MILLIS)));
        return training;
    }
}
