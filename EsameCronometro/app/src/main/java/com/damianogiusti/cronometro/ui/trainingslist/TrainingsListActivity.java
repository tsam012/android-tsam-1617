package com.damianogiusti.cronometro.ui.trainingslist;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;

import com.damianogiusti.cronometro.R;
import com.damianogiusti.cronometro.database.ChronometerContentProvider;

public class TrainingsListActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView trainingListView;
    private TrainingsAdapter trainingsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainings_list);
        bindViews();

        getLoaderManager().initLoader(0, null, this);

        trainingsAdapter = new TrainingsAdapter(this, null);
        trainingListView.setAdapter(trainingsAdapter);
    }

    private void bindViews() {
        trainingListView = (ListView) findViewById(R.id.trainingListView);
    }

    // Loader callbacks

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader = new CursorLoader(getApplicationContext(), ChronometerContentProvider.TRAININGS_URI,
                null, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        trainingsAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        trainingsAdapter.swapCursor(null);
    }
}
