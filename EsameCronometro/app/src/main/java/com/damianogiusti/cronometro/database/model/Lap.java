package com.damianogiusti.cronometro.database.model;

import java.util.Date;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class Lap {

    private long id;
    private Date date;
    private long durationMillis;
    private Training training;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getDurationMillis() {
        return durationMillis;
    }

    public void setDurationMillis(long durationMillis) {
        this.durationMillis = durationMillis;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }
}
