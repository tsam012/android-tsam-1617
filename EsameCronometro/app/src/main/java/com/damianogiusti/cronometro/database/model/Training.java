package com.damianogiusti.cronometro.database.model;

import java.util.Date;

/**
 * Created by Damiano Giusti on 13/06/17.
 */
public class Training {

    private long id;
    private Date date;
    private String location;
    private long totDurationMillis;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getTotDurationMillis() {
        return totDurationMillis;
    }

    public void setTotDurationMillis(long totDurationMillis) {
        this.totDurationMillis = totDurationMillis;
    }
}
