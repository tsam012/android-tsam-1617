package com.damianogiusti.cronometro.ui.chronometer;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.damianogiusti.cronometro.R;
import com.damianogiusti.cronometro.ui.HomeActivity;
import com.damianogiusti.cronometro.ui.utils.Utils;

import java.util.ArrayList;

public class ChronometerActivity extends Activity implements TimerListener {

    public static final String LOCATION_NAME_KEY_FOR_BUNDLE = "locationNameBundled";

    private static final String RUNNING_TIME_KEY_FOR_BUNDLE = "runningTimeBundled";
    private static final String LAST_LAP_TIME_KEY_FOR_BUNDLE = "lastLapTimeBundled";

    private TextView locationTextView;
    private TextView totalTimeTextView;
    private TextView runningTimeTextView;
    private Button lapButton;
    private Button stopButton;
    private Button cancelButton;

    private TimerFragment timerFragment;
    private int runningTime = 0;
    private int lastLapTime = 0;
    private String locationName;
    private ArrayList<Integer> lapsTimes = new ArrayList<>();

    private ChronometerController controller = new ChronometerController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chronometer);
        bindViews();

        controller.create(this);

        lapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLapButtonPressed();
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStopButtonPressed();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelButtonPressed();
            }
        });

        // get location name
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            throw new IllegalStateException("Must specify a location for a training");
        }
        locationName = extras.getString(LOCATION_NAME_KEY_FOR_BUNDLE);
        locationTextView.setText(locationName);

        // restore instance state
        if (savedInstanceState != null) {
            runningTime = savedInstanceState.getInt(RUNNING_TIME_KEY_FOR_BUNDLE, 0);
            lastLapTime = savedInstanceState.getInt(LAST_LAP_TIME_KEY_FOR_BUNDLE, 0);
            locationName = savedInstanceState.getString(LOCATION_NAME_KEY_FOR_BUNDLE, locationName);
        }

        // set first time
        runningTimeTextView.setText(Utils.formatTime(runningTime - lastLapTime));
        totalTimeTextView.setText(Utils.formatTime(false, runningTime));

        // init and start the timer
        FragmentManager fragmentManager = getFragmentManager();
        timerFragment = (TimerFragment) fragmentManager.findFragmentByTag(TimerFragment.TAG);
        if (timerFragment == null) {
            timerFragment = TimerFragment.newInstance();
            fragmentManager.beginTransaction()
                    .add(timerFragment, TimerFragment.TAG)
                    .commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(RUNNING_TIME_KEY_FOR_BUNDLE, runningTime);
        outState.putInt(LAST_LAP_TIME_KEY_FOR_BUNDLE, lastLapTime);
        outState.putString(LOCATION_NAME_KEY_FOR_BUNDLE, locationName);
    }

    private void bindViews() {
        locationTextView = (TextView) findViewById(R.id.locationTextView);
        totalTimeTextView = (TextView) findViewById(R.id.totalTimeTextView);
        runningTimeTextView = (TextView) findViewById(R.id.runningTimeTextView);
        lapButton = (Button) findViewById(R.id.lapButton);
        stopButton = (Button) findViewById(R.id.stopButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);
    }

    private void onLapButtonPressed() {
        lastLapTime = runningTime;
        lapsTimes.add(lastLapTime);
        runningTimeTextView.setText(Utils.formatTime(0));
    }

    private void onStopButtonPressed() {
        lastLapTime = runningTime;
        lapsTimes.add(lastLapTime);
        controller.chronometerStopped(runningTime, lapsTimes, locationName);
        stopTimerAndGoHome();
    }

    private void onCancelButtonPressed() {
        stopTimerAndGoHome();
    }

    @Override
    public void onBackPressed() {
        stopTimerAndGoHome();
    }

    private void stopTimerAndGoHome() {
        timerFragment.stop();
        goToHome();
    }

    private void goToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    // Timer Listener

    @Override
    public void onTimerTick(int progress) {
        runningTime = progress;
        int time = progress - lastLapTime;

        // update UI
        runningTimeTextView.setText(Utils.formatTime(time));
        totalTimeTextView.setText(Utils.formatTime(false, progress));
    }
}
