package it.damianogiusti.broadcastsender;

import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by Damiano Giusti on 20/01/17.
 */
public class EventBus {

    public static void init(Context context) {
        instance = new EventBus(context);
    }

    // --- singleton instance --- //
    private static EventBus instance = null;

    public static synchronized LocalBroadcastManager getInstance() {
        if (instance == null)
            throw new IllegalStateException("Call init() first.");
        return instance.localBroadcastManager;
    }
    // --- //

    private LocalBroadcastManager localBroadcastManager;

    private EventBus(Context context) {
        localBroadcastManager = LocalBroadcastManager.getInstance(context);
    }
}
