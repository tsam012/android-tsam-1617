package it.damianogiusti.downloadmanagertest;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Damiano Giusti on 10/02/17.
 */
public class CustomDownloadManager {

    private Context context;
    private AsyncHttpClient httpClient;

    public CustomDownloadManager(Context context) {
        this.context = context;
        this.httpClient = new AsyncHttpClient();
    }

    public void download(String url) {

        httpClient.get(context, url, new FileAsyncHttpResponseHandler(context) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {

            }
        });
    }
}
