package it.damianogiusti.downloadmanagertest;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String LINK = "http://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-8.7.1-amd64-netinst.iso";

    private Button btnStartDownload;
    private Button btnCancelDownload;

    private SystemDownloadManager downloadManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();

        btnStartDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStartDownloadRequested();
            }
        });

        btnCancelDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelDownloadRequested();
            }
        });

        downloadManager = new SystemDownloadManager(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        downloadManager.release(getApplicationContext());
    }

    private void onStartDownloadRequested() {
//        downloadManager.download(LINK);
        Intent intent = new Intent(this, DownloadService.class);
        Bundle bundle = new Bundle();
        bundle.putString(DownloadService.DOWNLOAD_URI, LINK);
        intent.putExtras(bundle);
        startService(intent);
    }

    private void onCancelDownloadRequested() {
        downloadManager.cancelIfRunning();
    }

    private void bindViews() {
        btnStartDownload = (Button) findViewById(R.id.btnStartDownload);
        btnCancelDownload = (Button) findViewById(R.id.btnCancelDownload);
    }
}
