package it.damianogiusti.downloadmanagertest;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;

/**
 * Created by Damiano Giusti on 10/02/17.
 */
public class SystemDownloadManager {

    private DownloadManager downloadManager;
    private long currentRequestID = Integer.MIN_VALUE;

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    public SystemDownloadManager(Context context) {
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        context.registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void download(String url) {
        if (!isDownloading())
            currentRequestID = downloadManager.enqueue(new DownloadManager.Request(Uri.parse(url)));
    }

    public void cancelIfRunning() {
        if (isDownloading()) {
            downloadManager.remove(currentRequestID);
            currentRequestID = Integer.MIN_VALUE;
        }
    }

    public boolean isDownloading() {
        return currentRequestID != Integer.MIN_VALUE;
    }

    public void release(Context context) {
        context.unregisterReceiver(downloadReceiver);
    }
}
