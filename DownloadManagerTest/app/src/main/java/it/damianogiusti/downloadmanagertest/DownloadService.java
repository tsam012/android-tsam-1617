package it.damianogiusti.downloadmanagertest;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import java.io.File;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Damiano Giusti on 10/02/17.
 */
public class DownloadService extends IntentService {

    private static final String TAG = "DownloadService";

    public static final String DOWNLOAD_URI = "it.damianogiusti.DOWNLOAD_URI";

    private SyncHttpClient httpClient;

    public DownloadService() {
        super("DownloadService");
        httpClient = new SyncHttpClient();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String url = intent.getExtras().getString(DOWNLOAD_URI).trim();
        if (!TextUtils.isEmpty(url)) {
            httpClient.get(getApplicationContext(), url, new FileAsyncHttpResponseHandler(getApplicationContext()) {
                @Override
                public void onStart() {
                    super.onStart();
                    Log.d(TAG, "onStart() called");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, File file) {
                    Log.d(TAG, "onSuccess() called with: statusCode = [" + statusCode + "], headers = [" + headers + "], file = [" + file + "]");
                }
            });
        }
    }
}
