package it.damianogiusti.threads;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

/**
 * Created by Damiano Giusti on 10/02/17.
 */
public class TimerFragment extends Fragment {

    public interface Callbacks {
        void onTimerUpdate(int count);
        void onTimerCanceled(int lastCount);
    }

    public static TimerFragment newInstance(Bundle bundle) {
        TimerFragment timerFragment = new TimerFragment();
        timerFragment.setArguments(bundle);
        return timerFragment;
    }


    private TimerFragment.Callbacks callbacks;
    private AsyncTimer asyncTimer;

    private TimerFragment.Callbacks notifier = new Callbacks() {
        @Override
        public void onTimerUpdate(int count) {
            if (callbacks != null)
                callbacks.onTimerUpdate(count);
        }

        @Override
        public void onTimerCanceled(int lastCount) {
            if (callbacks != null)
                callbacks.onTimerCanceled(lastCount);
        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof TimerFragment.Callbacks)
            callbacks = (TimerFragment.Callbacks) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);

        // Esisterà sempre una singola istanza di timer e sempre una singola istanza di fragment,
        // legate 1-1. Quando l'activity verrà distrutta, il fragment rimarrà in vita e al momento del
        // attach alla nuova activity, andrà a sovrascrivere le callback facendole puntare alla nuova activity.
        // In questo modo il timer manderà aggiornamenti solo al fragment, il quale saprà a quale activity inoltrarli.
        asyncTimer = new AsyncTimer(this);
        asyncTimer.execute();

        return null;
    }

    public AsyncTimer getAsyncTimer() {
        return asyncTimer;
    }

    private static class AsyncTimer extends AsyncTask<Void, Integer, Void> {

        private WeakReference<TimerFragment> weakFragment;
        private int counter;

        public AsyncTimer(TimerFragment fragment) {
            this.weakFragment = new WeakReference<>(fragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            counter = 0;
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (!isCancelled() && weakFragment.get() != null) {
                publishProgress(counter++);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            TimerFragment callbacks = weakFragment.get();
            if (callbacks != null)
                callbacks.notifier.onTimerUpdate(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            TimerFragment callbacks = weakFragment.get();
            if (callbacks != null)
                callbacks.notifier.onTimerCanceled(counter);
        }
    }
}
