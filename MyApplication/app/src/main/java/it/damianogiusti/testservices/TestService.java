package it.damianogiusti.testservices;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by Damiano Giusti on 27/01/17.
 */
public class TestService extends Service {

    public static final String ACTION_START_TIMER = "it.damianogiusti.testservices.START_TIMER";

    private static final String TAG = "TestService";

    private TestServiceBinder binder;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            timerLog();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service onCreate()");

        binder = new TestServiceBinder();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Service onBind()");

        // Servizio Bound
        // ricevo i paramentri in ingresso come extra dell'intent

        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service onStartCommand()");

        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(broadcastReceiver, new IntentFilter(ACTION_START_TIMER));

        // Servizio Started
        // ricevo i paramentri in ingresso come extra dell'intent

        Log.d(TAG, intent.getExtras() != null ? intent.getExtras().getString("hello", "") : "no extras");


        // Service.START_STICKY --> il servizio viene riavviato nel caso venga killato dal sistema, ma senza parametri nell'intent
        // Service.START_REDELIVER_INTENT --> il servizio viene riavviato con l'intent iniziale
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Service onDestroy()");
        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "Service onUnbind()");
        return super.onUnbind(intent);
    }

    private void timerLog() {
        try {
            for (int i = 0; i < 50; i++) {
                Thread.sleep(100);
                Log.d(TAG, "timerLog: " + (i + 1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        stopSelf();
    }

    public class TestServiceBinder extends Binder {
        public TestService getService() {
            return TestService.this;
        }
    }
}
