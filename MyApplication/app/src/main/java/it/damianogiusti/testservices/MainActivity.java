package it.damianogiusti.testservices;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Callable;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TestService";

    private TextView textView;
    private Button btn1;
    private Button btn2;

    private int counter;
    private boolean isBound;
    private Intent startServiceIntent;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected()");

            // Il metodo viene chiamato quando onBind ritorna un valore != null.
            // Se questo metodo non viene chiamato, l'activity non saprà mai che
            // il servizio non è avviato e non lo stopperà mai.
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected() called with: name = [" + name + "]");
            isBound = false;
        }
    };

    private BroadcastReceiver testIntentServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case TestIntentService.ACTION_TIMER_TICK:
                    textView.setText(String.valueOf(intent.getIntExtra("tick", -1)));
                    break;
                case TestIntentService.ACTION_TIMER_END:
                    Toast.makeText(MainActivity.this, "Timer end", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();

        startServiceIntent = new Intent(this, TestIntentService.class);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtn1Clicked();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtn2Clicked();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Activity onStart");

//        this.bindService(new Intent(this, TestService.class), serviceConnection, BIND_AUTO_CREATE);

        Bundle bundle = new Bundle();
        bundle.putString("hello", "Hello service, I'm your owner");
        startServiceIntent.putExtras(bundle);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TestIntentService.ACTION_TIMER_TICK);
        intentFilter.addAction(TestIntentService.ACTION_TIMER_END);
        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(testIntentServiceReceiver, intentFilter);

        this.startService(startServiceIntent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Activity onStop");

//        if (isBound)
//            this.unbindService(serviceConnection);

        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(testIntentServiceReceiver);

        this.stopService(startServiceIntent);
    }

    private void bindViews() {
        textView = (TextView) findViewById(R.id.textView);
        btn1 = (Button) findViewById(R.id.button1);
        btn2 = (Button) findViewById(R.id.button2);
    }

    private void onBtn1Clicked() {
        textView.setText("click " + counter++);
    }

    private void onBtn2Clicked() {
        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(new Intent());
    }
}
