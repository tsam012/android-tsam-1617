package it.damianogiusti.testservices;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by Damiano Giusti on 27/01/17.
 */
public class TestIntentService extends IntentService {

    public static final String ACTION_TIMER_TICK = "it.damianogiusti.testservices.TIMER_TICK";
    public static final String ACTION_TIMER_END = "it.damianogiusti.testservices.TIMER_END";

    private static final String TAG = "TestIntentService";

    public TestIntentService() {
        super("TestIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate() called");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() called");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // questo metodo viene chiamato in un worker thread

        // ricevo i paramentri in ingresso come extra dell'intent
        Log.d(TAG, intent.getExtras() != null ? intent.getExtras().getString("hello", "") : "no extras");

        timerLog();
        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(new Intent(ACTION_TIMER_END));
    }

    private void timerLog() {
        try {
            for (int i = 0; i < 50; i++) {
                Thread.sleep(100);
                Log.d(TAG, "timerLog: " + (i + 1));
                Intent intent = new Intent(ACTION_TIMER_TICK);
                intent.putExtra("tick", i + 1);
                LocalBroadcastManager.getInstance(getApplicationContext())
                        .sendBroadcast(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
