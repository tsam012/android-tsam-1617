package it.damianogiusti.baasboxdemo;

/**
 * Created by damianogiusti on 26/12/16.
 */

public final class RemoteUrls {

    public static final String BAASBOX_LOCAL_URL = "192.168.1.3";
    public static final int DEFAULT_PORT = 9000;
}
