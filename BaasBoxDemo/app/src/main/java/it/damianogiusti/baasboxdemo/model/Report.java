package it.damianogiusti.baasboxdemo.model;

import com.baasbox.android.BaasDocument;

import java.util.Date;

/**
 * Created by damianogiusti on 27/12/16.
 */

public class Report {

    private String id;
    private String owner;
    private Date creationDate;

    public static Report fromBaasDocument(BaasDocument document) {
        Report report = new Report();
        report.id = document.getId();
        report.owner = document.getString("owner");
        report.creationDate = new Date(document.getLong("creationDate"));
        return report;
    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
