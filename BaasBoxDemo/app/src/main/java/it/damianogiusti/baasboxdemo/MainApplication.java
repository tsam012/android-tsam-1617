package it.damianogiusti.baasboxdemo;

import android.app.Application;

import com.baasbox.android.BaasBox;

/**
 * Created by damianogiusti on 26/12/16.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        BaasBox.builder(getApplicationContext())
                .setApiDomain(RemoteUrls.BAASBOX_LOCAL_URL)
                .setPort(RemoteUrls.DEFAULT_PORT)
                .setAppCode("1234567890")
                .init();
    }
}
