package it.damianogiusti.baasboxdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasException;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;

import java.util.ArrayList;
import java.util.List;

import it.damianogiusti.baasboxdemo.controllers.BaasBoxController;
import it.damianogiusti.baasboxdemo.model.Report;

public class MainActivity extends AppCompatActivity {

    private static final String COLLECTION = "reports";

    private ListView listView;

    private BaasBoxController baasBoxController;
    private ReportAdapter reportAdapter;
    private List<Report> reportList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        baasBoxController = BaasBoxController.createForCollection(COLLECTION);

        reportList = new ArrayList<>(0);
        reportAdapter = new ReportAdapter(getApplicationContext(), reportList);

        listView.setAdapter(reportAdapter);

        baasBoxController.fetchAllDocuments(new BaasHandler<List<BaasDocument>>() {
            @Override
            public void handle(BaasResult<List<BaasDocument>> baasResult) {
                try {
                    List<BaasDocument> baasDocuments = baasResult.get();
                    if (baasDocuments != null) {
                        for (BaasDocument baasDocument : baasDocuments)
                            reportList.add(Report.fromBaasDocument(baasDocument));
                        reportAdapter.notifyDataSetChanged();
                    }
                } catch (BaasException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
