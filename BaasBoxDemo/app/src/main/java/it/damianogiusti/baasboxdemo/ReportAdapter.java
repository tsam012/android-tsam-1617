package it.damianogiusti.baasboxdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import it.damianogiusti.baasboxdemo.model.Report;

/**
 * Created by damianogiusti on 27/12/16.
 */

public class ReportAdapter extends BaseAdapter {

    private Context context;
    private List<Report> reportList;

    public ReportAdapter(Context context, List<Report> reportList) {
        this.context = context;
        this.reportList = reportList;
    }

    @Override
    public int getCount() {
        return reportList.size();
    }

    @Override
    public Report getItem(int position) {
        return reportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return reportList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        ReportViewHolder reportViewHolder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context).inflate(R.layout.report_list_row_layout, null);
            reportViewHolder = new ReportViewHolder();
            reportViewHolder.txtOwner = (TextView) rowView.findViewById(R.id.txtOwner);
            reportViewHolder.txtCreationDate = (TextView) rowView.findViewById(R.id.txtCreationDate);
            rowView.setTag(reportViewHolder);
        } else {
            reportViewHolder = (ReportViewHolder) rowView.getTag();
        }

        Report report = getItem(position);
        reportViewHolder.txtOwner.setText(report.getOwner());
        reportViewHolder.txtCreationDate.setText(report.getCreationDate().toString());

        return rowView;
    }

    private static class ReportViewHolder {
        private TextView txtOwner;
        private TextView txtCreationDate;
    }
}
