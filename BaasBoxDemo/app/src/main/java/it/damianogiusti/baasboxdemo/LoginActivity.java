package it.damianogiusti.baasboxdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.baasbox.android.BaasUser;

import it.damianogiusti.baasboxdemo.controllers.BaasBoxLoginController;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnLogin;

    private BaasBoxLoginController baasBoxLoginController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bind();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = txtUsername.getText().toString();
                final String password = txtPassword.getText().toString();
                if (TextUtils.isEmpty(username) && TextUtils.isEmpty(password))
                    baasBoxLoginController = new BaasBoxLoginController();
                else baasBoxLoginController = new BaasBoxLoginController(username, password);
                performLogin();
            }
        });
    }

    private void bind() {
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
    }

    private void performLogin() {
        baasBoxLoginController.loginUser(new BaasBoxLoginController.LoginCallbacks() {
            @Override
            public void onSuccess(BaasUser user) {
                Log.d(TAG, "Login succeeded for user " + user.getName());
                onLoginSucceeded();
            }

            @Override
            public void onFailure(String reason) {
                Log.d(TAG, "onFailure() called with: reason = [" + reason + "]");
            }
        });
    }

    private void onLoginSucceeded() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
