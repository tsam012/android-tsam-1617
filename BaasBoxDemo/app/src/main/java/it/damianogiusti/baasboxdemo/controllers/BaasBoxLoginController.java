package it.damianogiusti.baasboxdemo.controllers;

import android.support.annotation.NonNull;

import com.baasbox.android.BaasException;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;

/**
 * Created by damianogiusti on 26/12/16.
 */

public class BaasBoxLoginController {
    private static final String ADMIN_USERNAME = "admin";
    private static final String ADMIN_PASSWORD = /* WRITE THE PASSWORD HERE AS STRING */ ;

    public interface LoginCallbacks {
        void onSuccess(BaasUser user);

        void onFailure(String reason);
    }

    private String username;
    private String password;

    public BaasBoxLoginController() {
        this.username = ADMIN_USERNAME;
        this.password = ADMIN_PASSWORD;
    }

    public BaasBoxLoginController(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void loginUser(@NonNull final LoginCallbacks callbacks) {
        BaasUser baasUser = BaasUser.withUserName(username)
                .setPassword(password);
        baasUser.login(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                if (baasResult == null) {
                    callbacks.onFailure("result is null");
                    return;
                }

                if (baasResult.isFailed()) {
                    if (baasResult.error() != null)
                        callbacks.onFailure(baasResult.error().getMessage());
                    else callbacks.onFailure(baasResult.toString());
                    return;
                }

                try {
                    callbacks.onSuccess(baasResult.get());
                } catch (BaasException e) {
                    callbacks.onFailure(e.getMessage());
                }
            }
        });
    }
}
