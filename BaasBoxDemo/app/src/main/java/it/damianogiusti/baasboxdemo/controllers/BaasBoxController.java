package it.damianogiusti.baasboxdemo.controllers;

import android.text.TextUtils;

import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasHandler;

import java.util.List;

/**
 * Created by damianogiusti on 26/12/16.
 */
public class BaasBoxController {

    public static BaasBoxController createForCollection(String collectionName) {
        if (collectionName == null)
            return null;
        if (TextUtils.isEmpty(collectionName.trim()))
            return null;

        return new BaasBoxController(collectionName);
    }

    private final String collectionName;

    private BaasBoxController(String collectionName) {
        // prevent init
        this.collectionName = collectionName;
    }

    public BaasDocument createRandomDocument() {
        BaasDocument document = new BaasDocument(collectionName);
        document.put("owner", "Damiano Giusti");
        document.put("creationDate", System.currentTimeMillis());
        return document;
    }

    public void saveDocument(BaasDocument documentToSave,BaasHandler<BaasDocument> handler) {
        documentToSave.save(handler);
    }

    public void fetchAllDocuments(BaasHandler<List<BaasDocument>> handler) {
        BaasDocument.fetchAll(collectionName, handler);
    }
}
