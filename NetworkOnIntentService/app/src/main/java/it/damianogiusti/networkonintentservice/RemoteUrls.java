package it.damianogiusti.networkonintentservice;

/**
 * Created by Damiano Giusti on 23/02/17.
 */
public interface RemoteUrls {

    String SWAPI = "http://swapi.co/api/people";
    String GITHUB_API = "http://api.github.com/users/damianogiusti";
}
