package it.damianogiusti.networkonintentservice.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.util.TextUtils;

/**
 * Created by Damiano Giusti on 23/02/17.
 */
public class NetworkService extends IntentService {

    private static final String TAG = "NetworkService";

    public static final String URL_KEY = "url";

    public static final String SUCCESS = "success";
    public static final String RESPONSE_STATUS_CODE = "responseStatusCode";
    public static final String RESPONSE_BODY = "responseBody";

    private SyncHttpClient httpClient;
    private String currentURL;

    private AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            LocalBroadcastManager.getInstance(getApplicationContext())
                    .sendBroadcast(createIntent(true, statusCode, responseBody != null ? new String(responseBody) : ""));
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            LocalBroadcastManager.getInstance(getApplicationContext())
                    .sendBroadcast(createIntent(false, statusCode, responseBody != null ? new String(responseBody) : ""));
        }
    };

    public NetworkService() {
        super("NetworkService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        httpClient = new SyncHttpClient();

        Log.d(TAG, "onCreate() called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        currentURL = intent.getAction();
        if (!TextUtils.isEmpty(currentURL))
            new SyncHttpClient().get(this, currentURL, responseHandler);
    }

    private Intent createIntent(boolean success, int code, String body) {
        Intent intent = new Intent();
        intent.setAction(currentURL);

        Bundle bundle = new Bundle();
        bundle.putBoolean(SUCCESS, success);
        bundle.putInt(RESPONSE_STATUS_CODE, code);
        bundle.putString(RESPONSE_BODY, body);
        intent.putExtras(bundle);
        return intent;
    }
}
