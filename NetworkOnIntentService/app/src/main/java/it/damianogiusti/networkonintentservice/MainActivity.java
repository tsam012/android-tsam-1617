package it.damianogiusti.networkonintentservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import it.damianogiusti.networkonintentservice.services.NetworkService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Button btnCallFirstAPI;
    private Button btnCallSecondAPI;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getExtras().getBoolean(NetworkService.SUCCESS);
            String url = intent.getAction();
            String body = intent.getExtras().getString(NetworkService.RESPONSE_BODY);
            int statusCode = intent.getExtras().getInt(NetworkService.RESPONSE_STATUS_CODE);
            if (success) {
                Toast.makeText(MainActivity.this,
                        String.format("Request succeeded for url = [%s] with code [%d]: %s", url, statusCode, body),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this,
                        String.format("Request failed for url = [%s] with code [%d]", url, statusCode),
                        Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCallFirstAPI = (Button) findViewById(R.id.btnCallFirstAPI);
        btnCallSecondAPI = (Button) findViewById(R.id.btnCallSecondAPI);

        btnCallFirstAPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNetworkRequest(RemoteUrls.GITHUB_API);
            }
        });

        btnCallSecondAPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNetworkRequest(RemoteUrls.SWAPI);
            }
        });

        registerReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    private void sendNetworkRequest(String url) {
        Intent intent = new Intent(this, NetworkService.class);
        intent.setAction(url);
        startService(intent);
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RemoteUrls.GITHUB_API);
        intentFilter.addAction(RemoteUrls.SWAPI);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadcastReceiver, intentFilter);
    }

    private void unregisterReceiver() {
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(broadcastReceiver);
    }
}
