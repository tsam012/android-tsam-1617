package it.damianogiusti.contentprovider.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import it.damianogiusti.contentprovider.R;
import it.damianogiusti.contentprovider.database.ContactHelper;

/**
 * Created by damianogiusti on 23/12/16.
 */

public class ContactsCursorAdapter extends CursorAdapter {

    public ContactsCursorAdapter(Context context, Cursor c) {
        super(context, c, false);
        // Il terzo parametro dice di non fare il ricaricamento completo dei dati quando
        // i risultati cambiano, perchè la nuova query potrebbe essere effettuata sul main thread.
        // La nuova query la gestiremo noi con un Loader.
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact_list_item, parent, false);

        ContactViewHolder viewHolder = new ContactViewHolder();
        viewHolder.txtName = (TextView) view.findViewById(R.id.txtName);
        viewHolder.txtSurname = (TextView) view.findViewById(R.id.txtSurname);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ContactViewHolder viewHolder = (ContactViewHolder) view.getTag();
        String name = cursor.getString(cursor.getColumnIndex(ContactHelper.NAME));
        String surname = cursor.getString(cursor.getColumnIndex(ContactHelper.SURNAME));

        viewHolder.txtName.setText(name);
        viewHolder.txtSurname.setText(surname);
    }

    private static class ContactViewHolder {
        private TextView txtName;
        private TextView txtSurname;
    }
}
