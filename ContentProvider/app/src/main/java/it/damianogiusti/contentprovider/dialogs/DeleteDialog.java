package it.damianogiusti.contentprovider.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import it.damianogiusti.contentprovider.R;
import it.damianogiusti.contentprovider.database.ContactsRepository;
import it.damianogiusti.contentprovider.database.ContactsRepositoryContentResolver;
import it.damianogiusti.contentprovider.model.Contact;

/**
 * Created by damianogiusti on 23/12/16.
 */

public class DeleteDialog extends DialogFragment {

    private static final String CONTACT_ID_KEY_FOR_BUNDLE = "ContactIdBundled";

    public static DeleteDialog newInstance(long contactId) {
        DeleteDialog deleteDialog = new DeleteDialog();
        Bundle bundle = new Bundle();
        bundle.putLong(CONTACT_ID_KEY_FOR_BUNDLE, contactId);
        deleteDialog.setArguments(bundle);
        return deleteDialog;
    }

    public interface DeleteDialogCallbacks {
        void onDeletionConfirmed(Contact contact);
    }

    private ContactsRepository contactsRepository;
    private DeleteDialogCallbacks deleteDialogCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof DeleteDialogCallbacks)
            deleteDialogCallbacks = (DeleteDialogCallbacks) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        contactsRepository = new ContactsRepositoryContentResolver(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        long contactId = getArguments().getLong(CONTACT_ID_KEY_FOR_BUNDLE, -1);
        if (contactId > -1) {
            final Contact contactToDelete = contactsRepository.getContactById(contactId);
            builder.setMessage(getString(R.string.delete_confirmation, contactToDelete.getDisplayName()));

            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (deleteDialogCallbacks != null)
                        deleteDialogCallbacks.onDeletionConfirmed(contactToDelete);
                }
            });
        }

        return builder.create();
    }
}
