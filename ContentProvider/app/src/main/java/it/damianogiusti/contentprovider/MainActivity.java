package it.damianogiusti.contentprovider;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import it.damianogiusti.contentprovider.adapter.ContactsCursorAdapter;
import it.damianogiusti.contentprovider.database.ContactsContentProvider;
import it.damianogiusti.contentprovider.database.ContactsRepository;
import it.damianogiusti.contentprovider.database.ContactsRepositoryContentResolver;
import it.damianogiusti.contentprovider.dialogs.DeleteDialog;
import it.damianogiusti.contentprovider.dialogs.UpdateDialog;
import it.damianogiusti.contentprovider.model.Contact;

public class MainActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        UpdateDialog.UpdateDialogCallbacks,
        DeleteDialog.DeleteDialogCallbacks {

    private static final int ACTIVITY_LOADER_ID = 1;

    private ListView listView;
    private Button btnAddContact;
    private ContactsCursorAdapter contactsCursorAdapter;
    private ContactsRepository contactsRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.contactsListView);
        btnAddContact = (Button) findViewById(R.id.btnAddContact);

        contactsRepository = new ContactsRepositoryContentResolver(getApplicationContext());
        contactsCursorAdapter = new ContactsCursorAdapter(this, null);

        listView.setAdapter(contactsCursorAdapter);
        getLoaderManager().initLoader(ACTIVITY_LOADER_ID, null, this);

        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertContact();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateById(id);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deleteById(id);
                return true;
            }
        });
    }

    private void insertContact() {
        Contact contact = Contact.randomContact();
        contactsRepository.addContact(contact);
    }

    private void updateById(long contactId) {
        UpdateDialog.newInstance(contactId).show(getFragmentManager(), UpdateDialog.class.getSimpleName());
    }

    private void deleteById(long contactId) {
        DeleteDialog.newInstance(contactId).show(getFragmentManager(), DeleteDialog.class.getSimpleName());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, ContactsContentProvider.CONTACTS_URI,
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        contactsCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        contactsCursorAdapter.swapCursor(null);
    }

    @Override
    public void onUpdateRequested(Contact contact) {
        contactsRepository.updateContact(contact);
    }

    @Override
    public void onDeletionConfirmed(Contact contact) {
        contactsRepository.removeContact(contact);
    }
}
