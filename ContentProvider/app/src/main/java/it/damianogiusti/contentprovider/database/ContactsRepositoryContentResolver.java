package it.damianogiusti.contentprovider.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.List;

import it.damianogiusti.contentprovider.model.Contact;

/**
 * Created by damianogiusti on 23/12/16.
 */

public class ContactsRepositoryContentResolver implements ContactsRepository {

    private Context context;

    public ContactsRepositoryContentResolver(Context context) {
        this.context = context;
    }

    @Override
    public List<Contact> getContacts() {
        return null;
    }

    @Override
    public boolean removeAllContacts() {
        return false;
    }

    @Override
    public Contact getContactById(long contactId) {
        Cursor cursor = context.getContentResolver().query(ContactsContentProvider.CONTACTS_URI,
                null, String.format("%s = %d", ContactHelper._ID, contactId), null, null);
        if (cursor.getCount() != 1)
            throw new AssertionError();
        Contact contact = new Contact();
        while (cursor.moveToNext()) {
            contact.setId(contactId);
            contact.setName(cursor.getString(cursor.getColumnIndex(ContactHelper.NAME)));
            contact.setSurname(cursor.getString(cursor.getColumnIndex(ContactHelper.SURNAME)));
        }
        cursor.close();
        return contact;
    }

    @Override
    public Contact addContact(Contact contact) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContactHelper.NAME, contact.getName());
        contentValues.put(ContactHelper.SURNAME, contact.getSurname());

        context.getContentResolver().insert(ContactsContentProvider.CONTACTS_URI, contentValues);
        return contact;
    }

    @Override
    public boolean removeContact(Contact contact) {
        int deletedRows = context.getContentResolver().delete(ContactsContentProvider.CONTACTS_URI,
                String.format("%s = %d", ContactHelper._ID, contact.getId()), null);
        return deletedRows == 1;
    }

    @Override
    public boolean updateContact(Contact contact) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContactHelper.NAME, contact.getName());
        contentValues.put(ContactHelper.SURNAME, contact.getSurname());

        int updatedRows = context.getContentResolver().update(ContactsContentProvider.CONTACTS_URI, contentValues,
                String.format("%s = %d", ContactHelper._ID, contact.getId()), null);
        return updatedRows == 1;
    }
}
