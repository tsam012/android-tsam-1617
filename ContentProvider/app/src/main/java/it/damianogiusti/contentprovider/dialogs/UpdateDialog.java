package it.damianogiusti.contentprovider.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import it.damianogiusti.contentprovider.R;
import it.damianogiusti.contentprovider.database.ContactsRepository;
import it.damianogiusti.contentprovider.database.ContactsRepositoryContentResolver;
import it.damianogiusti.contentprovider.model.Contact;

/**
 * Created by damianogiusti on 23/12/16.
 */

public class UpdateDialog extends DialogFragment {

    private static final String CONTACT_ID_KEY_FOR_BUNDLE = "ContactIdBundled";

    public static UpdateDialog newInstance(long contactId) {
        UpdateDialog updateDialog = new UpdateDialog();
        Bundle bundle = new Bundle();
        bundle.putLong(CONTACT_ID_KEY_FOR_BUNDLE, contactId);
        updateDialog.setArguments(bundle);
        return updateDialog;
    }

    public interface UpdateDialogCallbacks {
        void onUpdateRequested(Contact contact);
    }

    private ContactsRepository contactsRepository;
    private UpdateDialogCallbacks updateDialogCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof UpdateDialogCallbacks)
            updateDialogCallbacks = (UpdateDialogCallbacks) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        contactsRepository = new ContactsRepositoryContentResolver(getActivity());
        final Contact contact = contactsRepository.getContactById(getArguments().getLong(CONTACT_ID_KEY_FOR_BUNDLE));
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.update_dialog_layout, null);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        final EditText etxtName = (EditText) view.findViewById(R.id.etxtName);
        final EditText etxtSurname = (EditText) view.findViewById(R.id.etxtSurname);
        etxtName.setText(contact.getName());
        etxtSurname.setText(contact.getSurname());

        dialogBuilder.setView(view);

        dialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contact.setName(etxtName.getText().toString());
                contact.setSurname(etxtSurname.getText().toString());
                if (updateDialogCallbacks != null)
                    updateDialogCallbacks.onUpdateRequested(contact);
            }
        });

        return dialogBuilder.create();
    }
}
